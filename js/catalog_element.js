var propsTree = {
    'PROP_121_2':{
        'SUB_NAME':'PROP_20',
        'SUB_PROP':{
            'PROP_20_2':'2'
        }
    },
    'PROP_121_4':{
        'SUB_NAME':'PROP_20',
        'SUB_PROP':{
            'PROP_20_2':'2'
        }
    },
    'PROP_20_2':{
        'SUB_NAME':'PROP_21',
        'SUB_PROP':{
            'PROP_21_5':'5',
            'PROP_21_6':'6'
        }
    },
    'PROP_21_5':{
        'END':'1'
    },
    'PROP_121_5':{
        'SUB_NAME':'PROP_21',
        'SUB_PROP':{
            'PROP_21_5':'5',
            'PROP_21_6':'6',
            'PROP_21_6':'7'
        }
    },
    'PROP_21_6':{
        'END':'1'
    },
    'PROP_121_6':{
        'SUB_NAME':'PROP_20',
        'SUB_PROP':{
            'PROP_20_2':'2',
            'PROP_20_3':'3'
        }
    },
    'PROP_20_3':{
        'SUB_NAME':'PROP_21',
        'SUB_PROP':{
            'PROP_21_7':'7'
        }
    },
    'PROP_21_7':{
        'END':'1'
    },
    'PROP_121_10':{
        'SUB_NAME':'PROP_20',
        'SUB_PROP':{
            'PROP_20_2':'2'
        }
    }
};

var elementProrpId =
{
    'PROP_121_2|PROP_20_2|PROP_21_5':2685,
    'PROP_121_2|PROP_20_2|PROP_21_6':2169,
    'PROP_121_2|PROP_20_3|PROP_21_7':2175,
    'PROP_121_4|PROP_20_2|PROP_21_6':2173,
    'PROP_121_5|PROP_21_5':2703,
    'PROP_121_5|PROP_21_6':2171,
    'PROP_121_5|PROP_21_7':2178,
    'PROP_121_6|PROP_20_2|PROP_21_5':2702,
    'PROP_121_6|PROP_20_2|PROP_21_6':2172,
    'PROP_121_6|PROP_20_3|PROP_21_7':2176,
    'PROP_121_10|PROP_20_2|PROP_21_5':2701,
    'PROP_121_10|PROP_20_2|PROP_21_6':2170,
    'PROP_121_10|PROP_20_3|PROP_21_7':2177
}

function clickProp (prop_offer){
    if(prop_offer){
        $("a[data-prop_value="+prop_offer+"]").click();
    }
}

function iDElementSelect() {
    var arPror = [];
    var strProp = "";
    $("div.choice__item a.active").each(function(i,elem) {
        arPror[i] = $(elem).data("prop_value");
    });
    var str = arPror.join('|');

    var idDefoult = elementProrpId[str];

    console.log(idDefoult);

    $(".product__slider_wrap").hide();
    if(idDefoult){

        $("#slide_"+idDefoult).show();


        return true;
    }else{
        return false;
    }

}

$(document).ready(function() {

    var first_click;
    for(i in propsTree) {
        if (!first_click) {
            first_click = i;
        }
    }
    clickProp(first_click);
    iDElementSelect();
});


$(".choice__item_btns a").click(function(){
    if(!$(this).hasClass("active")) {
        $(this).siblings("a").removeClass("active");
        $(this).addClass("active");
        var prop = $(this).closest(".choice__item").data("prop");
        var propvalue = $(this).data("prop_value");
        var end = false;
        while(!end){
            var curProp = propsTree[propvalue];
            if(curProp.END == "1"){
                end = true
            }
            else{
                var div = $("div[data-prop="+curProp.SUB_NAME+"]");
                div.find("a").hide();
               // div.hide();
                var first = true;
                //$("div[data-prop="+curProp.SUB_NAME+"]").show();
                for(key in curProp.SUB_PROP){
                    if(first){
                        propvalue = key;
                        first = false;
                        div.find("[data-prop_value="+key+"]").addClass("active").siblings("a").removeClass("active");
                    }
                    div.find("[data-prop_value="+key+"]").show();
                }
            }
        }
        iDElementSelect();
    }
    return false;
    //vse scrit
    // pokazat  slide_id
});