$(document).ready(function() {
    var popupOpened = false;/*3*/
    $(".nano").nanoScroller();

    /*боковое меню*/

    $("body").on("click", ".js-menu__btn", function() {
        if(!$(".wrapper").hasClass("active")) {
            $(".wrapper").addClass("active");
            $(".header").addClass("active");
            return false;
        }
    });

    $("body").on("click", ".wrapper.active .content", function() {
        $(".wrapper").removeClass("active");
        setTimeout(function() {
            $(".header").removeClass("active");
        }, 300);
        return false;
    });

    /* клик на избранное */

    $("body").on("click", ".js-favorites__btn", function() {
        $(this).toggleClass("active");
        return false;
    });


    /* попапы */

    $("body").on("click", ".item__smart .product__stickers a", function() {
        popupOpened = true;
        var popupLink = "." + $(this).attr("data-popup");
        $(popupLink).show();
        $(".overlay").show();
        history.pushState({}, "", location.href);/*3*/
        return false;
    });

    $(window).on("popstate", function(e) {/*3*/
        if(popupOpened) {
            closePopup();
        }
    });

    $("body").on("click", ".stickers__popup .js-close__btn", function() {
        window.history.back();
        closePopup();
    });

    $("body").on("click", function(e) {
        if(!$(".stickers__popup").is(e.target) && $(".stickers__popup").has(e.target).length === 0) {
            if(popupOpened) {
                window.history.back();
            };
            closePopup();
        };
    });


    function closePopup() {
        popupOpened = false;
        $(".stickers__popup").hide();
        $(".overlay").hide();
    };
    /* количество товара, добавляемое в корзину*/

    $("body").on("click", ".js-product__minus", function () {
        var $input = $(this).parent().find('input');
        var count = parseInt($input.val()) - 1;
        if(count < 1) {
            count = 1 ;
        }
        $input.val(count);
        $input.change();
        return false;
    });

    $("body").on("click", ".js-product__plus", function () {
        var $input = $(this).parent().find('input');
        $input.val(parseInt($input.val()) + 1);
        $input.change();
        return false;
    });

    /* блок выбора папаметров товара */

    $("body").on("click", ".js-purchase__btn, .js-select, .js-config", function() {
        if(!$(".item__smart_product").hasClass("active")) {
            $(".item__smart_product").addClass("active");
            $(".product__choice, .delivery__info").slideDown("slow");
            $(".product__tabs_block, .purchase__block_top").slideUp("slow");
            $(".product__rating, .product__stickers").hide();
            $('.product__slider-small').slick('setPosition');
           return false;
        };
        return false;
    });

    $("body").on("click", ".product__choice_close .js-close_btn", function() {
        $(".item__smart_product").removeClass("active");
        $(".product__choice, .delivery__info").slideUp("normal");
        $(".product__tabs_block, .purchase__block_top").slideDown("normal");
        $(".product__rating, .product__stickers").show()
        return false;
    });

    /* добавить ещё в корзину */

    $("body").on("click", ".js-in-basket__btn", function() {
       $(this).text("Добавить ещё");
        return false;
    });

    /*замена больших картинок на слайдере аксесуаров*/

    $("body").on("click", ".accessories__slide_img img", function() {
        var newSrc = $(this).attr("data-src");
        $(this).closest(".prod__accessories_item").find(".accessories__item_img img").attr("src", newSrc);
    });

    /* табы на детальной */

    $("body").on("click", ".product__tabs_item", function() {
        var dataTab = "." + $(this).attr("data-tab");
        if($(this).hasClass("active")) {
            $(this).removeClass("active")
            $(dataTab).slideUp("normal");
        } else {
            $(".product__tabs_content").slideUp("normal");
            $(".product__tabs_item").removeClass("active");
            $(this).addClass("active");
            $(dataTab).slideDown("normal");
        };
    });

    /* детальная аксессуаров слайдеры */

    $(".accessories__product").find(".product__slider:not(:first-of-type)").hide();
    $("body").on("click",".accessories__models_img img", function() {
        var dataModel = $(this).attr("data-model");
        $(".accessories__product").find(".product__slider").each(function() {
            if($(this).attr("data-model") == dataModel) {
                $(".accessories__product").find(".product__slider").hide();
                $(this).show().slick('setPosition');
            }
        });
    });

    /* скролл к отзывам *//*2*/

    $("body").on("click", ".rating__text", function() {
        $(".prod__reviews_anchor").click();
        var top = $(".product__tabs_block").offset().top;
        $('body,html').animate({scrollTop: top - 70}, 500);
    });

    /* скролл к верху страницы, если раскрыть выбор оффера */
    $("body").on("click", ".js-purchase__btn, .js-select, .js-config", function() {
        $('body,html').animate({scrollTop: 0}, 500);
    });
    
    /* открытие формы отзывов */

    $("body").on("click", ".add-review__btn", function() {
        $(this).hide().siblings("form").show();
        return false;
    })

    /*проверка формы отзывов*//*5*/
    $("#reviews__form_photo").on("change", function() {
        $(".upload-error_message").hide().removeClass("visible");
        var size = 0;
        if(this.files.length > 5) {
            $(".upload-error_message").show().addClass("visible");
        } else if (this.files.length) {
            for(var i = 0; i < this.files.length; i++){
                size += this.files[i].size;
            }
            if (size > 31457280) {
                $(".upload-error_message").show().addClass("visible");
            }
        };
    });

    $(".reviews__form form").submit(function() {
        return validate();
    });

    /*выпадающее меню пагинации в отзывах*//*5*/

    $("body").on("click", ".show-more__btn", function() {
        $(".overlay").show();
        return false;
    });


    $("body").on("click", ".reviews__pagination_link", function() {
        $(".reviews__pagination_menu").show();
        $(".nano").nanoScroller();
        return false;
    });

    $("body").on("click", function(e) {
        if(!$(".reviews__pagination_menu").is(e.target) && $(".reviews__pagination_menu").has(e.target).length === 0) {
            $(".reviews__pagination_menu").hide();
        };
    });

    /* галерея в отзывах */ /*6*/
   /* $("body").on("click",".loaded__photo_item  img", function() {
        startGallery();
    });*/


});

/*5*/
function validate() {
    var userName = document.forms["rv-form"]["guest-name"];
    var userMessage = document.forms["rv-form"]["review-text"];
    var valide = true;
    $(".reviews__form form input").removeClass('error-border');
    $(".reviews__form form textarea").removeClass('error-border');

    if (userName && userName.value.length == 0) {
        valide = false;
        $(userName).addClass("error-border");
    }

    if (userMessage && userMessage.value.length == 0) {
        valide = false;
        $(userMessage).addClass("error-border");
    }

    if ($(".upload-error_message").hasClass("visible")) {
        valide = false;
    }

    if (!valide) return false;
}

function startGallery() {/*6*/
    var pswpElement = document.querySelectorAll('.pswp')[0];

    // build items array
    var items = [
        {
            src: 'img/user-photo1.jpg',
            w: 600,
            h: 400
        },
        {
            src: 'img/user-photo2.jpg',
            w: 1200,
            h: 900
        },
        {
            src: 'img/user-photo3.jpg',
            w: 1200,
            h: 900
        }
    ];

    // define options (if needed)
    var options = {
        // optionName: 'option value'
        // for example:
        index: 0, // start at first slide
        mainClass: 'pswp--minimal--dark',
        barsSize: {top: 0, bottom: 0},
        captionEl: false,
        fullscreenEl: false,
        shareEl: false,
        bgOpacity: 0.85,
        tapToClose: true,
        tapToToggleControls: false,
    };

    // Initializes and opens PhotoSwipe
    var gallery = new PhotoSwipe( pswpElement, PhotoSwipeUI_Default, items, options);
    gallery.init();
}

